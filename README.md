# cats-ex-sop

Example usage of the CATS framework. Implementation of the SOP anytime forward tree search.


## Installation and compilation

This project requires to be positioned next to [cats-framework](https://gitlab.com/librallu/cats-framework) v0.3

 1. `mkdir ~/cats-dev ; cd ~/cats-dev`
 2. `git clone https://gitlab.com/librallu/cats-framework`
 3. `cd cats-framework/ ; git checkout tags/v0.3 ; cd ..`
 4. `git clone https://gitlab.com/librallu/cats-ex-sop` 
 5. `cd cats-ex-sop/`
 6. `cmake . ; make`


## Running the executable

This project produces the following executable

```
runExperiment.exe INST_FILE TIME_LIMIT
```

Example usage:
```
./runExperiment.exe insts/R.700.1000.15.sop 30
```