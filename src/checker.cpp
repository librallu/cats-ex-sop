#include <iostream>
#include <set>
#include <omp.h>
#include <signal.h>

#include "Instance.hpp"
#include "checker.hpp"

using namespace exsop;


int main(int argc, char* argv[]) {
    if ( argc < 3 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE_NAME SOLUTION_FILENAME" << std::endl;
        return 1;
    }

    // parse user input
    Instance inst(argv[1]);

    // create solution
    std::vector<NodeId> sol;
    std::ifstream f;
    f.open(argv[2]);
    int tmp;
    while (f >> tmp) {
        sol.push_back(tmp);
    }
    f.close();

    // call checker
    int64_t checker_result = checker(inst, sol);
    std::cout << checker_result << std::endl;
    if ( checker_result < 0 ) {
        return -checker_result;
    }
    return 0;
}
