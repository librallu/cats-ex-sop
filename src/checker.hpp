#pragma once

#include <vector>

#include "Instance.hpp"

namespace exsop {

/**
 * returns 
 * -1 if solution has not n vertices
 * -2 if solution does not start by s and end by t
 * -3 if a precedence constraint is violated
 * -4 if a vertex appears twice
 */
int64_t checker(Instance& inst, std::vector<NodeId> sol) {
    // check if size respected
    if ( static_cast<int>(sol.size()) != inst.get_n() ) {
        return -1;
    }
    // check if start and end not respected
    if ( sol[0] != 0 && sol[sol.size()-1] != sol.size()-1 ) {
        return -2;
    }
    // check precedence constraints
    std::vector<int> positions(sol.size(), 0);
    for ( uint32_t i = 0 ; i < sol.size() ; i++ ) {
        positions[sol[i]] = i;
    }
    for ( Precedence p : inst.get_precedences() ) {
        if ( positions[p.first] > positions[p.second] ) return -3;
    }
    // check that all vertices are in the solution
    std::vector<bool> added(sol.size(), false);
    for ( NodeId e : sol ) {
        if ( added[e] ) {
            return -4;
        }
        added[e] = true;
    }
    // if the solution is feasible, compute cost
    int64_t cost = 0;
    for ( uint32_t i = 1 ; i < sol.size() ; i++ ) {
        cost += inst.get_weight(sol[i-1], sol[i]);
    }
    return cost;
}

}  // namespace exsop
