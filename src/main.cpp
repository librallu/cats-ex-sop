#include <iostream>
#include <set>
#include <omp.h>
#include <signal.h>

#include "../../cats-framework/include/util/includeAlgorithms.hpp"
#include "../../cats-framework/include/SearchManager.hpp"

#include "Instance.hpp"
#include "NodeForward.hpp"

using namespace cats;
using namespace exsop;


int main(int argc, char* argv[]) {
    if ( argc < 4 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE_NAME TIME_LIMIT SOLUTION_FILE" << std::endl;
        return 1;
    }

    // parse user input
    Instance inst(argv[1]);
    int time_limit = std::stoi(argv[2]);
    std::string solution_filename = argv[3];

    // defines the search manager and the prefix equivalence store
    SearchManager search_manager;
    GenericPEStoreHash<PE_SOP, nodeEqHash, DominanceInfos2> prefix_equivalence_store;


    // define root of the tree
    NodePtr root_ptr = NodePtr(new NodeForward(inst, solution_filename));

    // measure some node openings, etc.
    root_ptr = NodePtr(new StatsCombinator<PE_SOP>(root_ptr, search_manager, search_manager.getSearchStats(), false));

    // add bounding combinator
    // root_ptr = NodePtr(new BBCombinator<PE_SOP>(root_ptr, search_manager));

    // add the prefix equivalence domination combinator
    root_ptr = NodePtr(new DominanceCombinator2<PE_SOP>(root_ptr, search_manager, prefix_equivalence_store));

    // construct the tree search algorithm
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    // auto ts = DFS(ts_params);
    auto ts = IterativeBeamSearch(ts_params, 1, 2, true);
    // auto ts = IterativeMBAStar(ts_params, 1, 2);
    // auto ts = LDS(ts_params);

    // start the search
    search_manager.start();
    ts.run(time_limit);

    // display statistics and write performance profile file
    search_manager.printStats();
    prefix_equivalence_store.printStats();
    // search_manager.writeAnytimeCurve("perfprofile_dfs.json", "DFS");
    // search_manager.writeAnytimeCurve("perfprofile_ibs.json", "Iterative Beam Search");
    // search_manager.writeAnytimeCurve("perfprofile_mba.json", "MBA*");
    // search_manager.writeAnytimeCurve("perfprofile_lds.json", "LDS");

    return 0;
}
