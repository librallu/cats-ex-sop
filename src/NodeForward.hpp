#pragma once

#include <iostream>
#include <cassert>
#include <string>
#include <boost/functional/hash.hpp>

#include "../../cats-framework/include/Node.hpp"
#include "../../cats-framework/include/util/SubsetInt.hpp"

#include "Instance.hpp"
#include "checker.hpp"

using namespace cats;

namespace exsop {

/**
 * SOP prefix equivalence
 */
struct PE_SOP {
    SubsetInt subset;
    int last;

    PE_SOP(const PE_SOP& n) :
        subset(n.subset), last(n.last) {}
    PE_SOP(const SubsetInt sub, int l) : subset(sub), last(l) {}

    bool operator==(const PE_SOP& a) const {
        if ( last != a.last ) return false;
        return subset == a.subset;
    }
};

/**
 * hash function taking a nodeEquivalenceSOP as a parameter
 */
struct nodeEqHash {
    size_t operator()(const PE_SOP& n) const noexcept {
        size_t seed = n.subset.hash();
        boost::hash_combine(seed, static_cast<size_t>(n.last));
        return seed;
    }
};

class NodeForward : public PrefixEquivalenceNode<PE_SOP> {
 private:
    Instance& inst_;
    std::vector<NodeId> prefix_;
    Weight cost_prefix_;
    SubsetInt added_subset_;
    std::string& sol_filename_;

 public:
    explicit NodeForward(Instance& inst, std::string& sol_filename): Node(), inst_(inst), cost_prefix_(0), added_subset_(), sol_filename_(sol_filename) {
        prefix_.push_back(inst_.get_start_vertex());
        added_subset_.add(inst_.get_start_vertex());
    }

    explicit NodeForward(const NodeForward& s): Node(s), inst_(s.inst_), prefix_(s.prefix_), cost_prefix_(s.cost_prefix_), added_subset_(s.added_subset_), sol_filename_(s.sol_filename_) {}

    inline NodePtr copy() const override { return NodePtr(new NodeForward(*this)); }

    inline double evalPrefix() const override {
        return cost_prefix_;
    }

    inline double guide() override {
        return cost_prefix_;
    }

    inline std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        NodeId last_city_ = lastCity();
        // for each vertex, check if we can add it
        for ( NodeId neigh : inst_.get_possible_successors(last_city_) ) {
            // check that it is not already added and preds satisfied
            bool to_add = !added_subset_.contains(neigh);
            // check that precedences are satisfied
            if ( to_add ) {
                for ( NodeId u : inst_.get_predecessors(neigh) ) {
                    if ( !added_subset_.contains(u) ) {
                        to_add = false;
                        break;
                    }
                }
            }
            if ( to_add ) {  // generate children and add it to the result
                NodeForward* child = new NodeForward(*this);
                child->addCity(neigh);
                res.push_back(NodePtr(child));
            }
        }
        return res;
    }

    inline bool isGoal() const override {
        return inst_.get_n() == static_cast<int>(prefix_.size());
    }

    /**
     * called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        // double check_val = checker(inst_, prefix_);
        // assert(check_val == this->evaluate());
        // write solution file
        std::ofstream f;
        f.open(sol_filename_);
        for ( NodeId p : prefix_ ) {
            f << p << " ";
        }
        f << "\n";
        f.close();
    }

    inline PE_SOP getPrefixEquivalence() const override {
        return PE_SOP(added_subset_, lastCity());
    }

    std::string getName() override {
        return "forward";
    }

    inline NodeId lastCity() const { return prefix_.back(); }

 private:
    /**
     * adds a given city to the current node
     */
    inline void addCity(NodeId j) {
        NodeId i = lastCity();
        added_subset_.add(j);
        prefix_.push_back(j);
        cost_prefix_ += inst_.get_weight(i, j);
    }
};

}  // namespace exsop
