#pragma once

#include <vector>
#include <fstream>
#include <exception>

namespace exsop {

typedef uint16_t NodeId;
typedef int32_t Weight;
typedef std::pair<NodeId, NodeId> Precedence;


/**
 * fired when a file is not found while trying to open it
 */
class FileNotFoundException: public std::exception {
  virtual const char* what() const throw() {
    return "File not found exception";
  }
};

/**
 * \brief represents a SOP instance
 * the constructor parses a sop file
 */
class Instance {
 private:
    int n_;
    std::vector<std::vector<Weight>> matrix_;
    std::vector<Precedence> precedences_;
    std::vector<std::vector<NodeId>> possible_successors_;
    std::vector<std::vector<NodeId>> predecessors_;


 public:
    explicit Instance(std::string filename) {
        // reads the instance file
        std::ifstream f;
        f.open(filename);
        if ( !f.is_open() ) {
            throw FileNotFoundException();
        }
        f >> n_;
        possible_successors_ = std::vector<std::vector<NodeId>>(n_, std::vector<NodeId>());
        predecessors_ = std::vector<std::vector<NodeId>>(n_, std::vector<NodeId>());
        for ( int i = 0 ; i < n_ ; i++ ) {
            std::vector<Weight> tmp;
            for ( int j = 0 ; j < n_ ; j++ ) {
                Weight tmp_v;
                f >> tmp_v;
                if ( tmp_v < 0 ) {
                    precedences_.push_back(Precedence(j, i));
                    predecessors_[i].push_back(j);
                } else {
                    possible_successors_[i].push_back(j);
                }
                tmp.push_back(tmp_v);
            }
            matrix_.push_back(tmp);
        }
    }

    inline NodeId get_n() const { return n_; }

    inline Weight get_weight(NodeId i, NodeId j) const { return matrix_[i][j]; }

    inline std::vector<NodeId>& get_possible_successors(NodeId i) { return possible_successors_[i]; }

    inline std::vector<NodeId>& get_predecessors(NodeId i) { return predecessors_[i]; }

    inline std::vector<Precedence>& get_precedences() { return precedences_; }

    inline NodeId get_start_vertex() const { return 0; }

    inline NodeId get_end_vertex() const { return n_-1; }

};

}  // namespace exsop
